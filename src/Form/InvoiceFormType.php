<?php

namespace App\Form;


use App\Entity\Currency;
use App\Entity\Invoice;
use App\Entity\InvoiceAccountingType;
use App\Entity\InvoiceItem;
use App\Entity\InvoiceStatus;
use App\Entity\InvoiceType;
use App\Entity\Payment;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ButtonType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\RadioType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class InvoiceFormType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $this->contacts = $options['contacts'];



        $builder
            ->add('status', EntityType::class, [
                'class' => InvoiceStatus::class,
                'choice_value' => 'id',
                'choice_label' => 'status_name',
                'label_attr' => [
                    'class' => 'btn btn-outline-primary radio-margin-negate'
                ],
                'choice_attr' => function($choice, $key, $value) {
                    // adds a class like attending_yes, attending_no, etc
                    return ['class' => 'btn-check'];
                },
                'expanded' => true,
                'multiple' => false,
            ])
            ->add('invoice_type', EntityType::class, [
                'class' => InvoiceType::class,
                'choice_value' => 'id',
                'choice_label' => 'type_name',
                'label_attr' => [
                    'class' => 'btn btn-outline-primary radio-margin-negate'
                ],
                'choice_attr' => function($choice, $key, $value) {
                    // adds a class like attending_yes, attending_no, etc
                    return ['class' => 'btn-check'];
                },
                'expanded' => true,
                'multiple' => false,
            ])
            ->add('invoice_accounting_type', EntityType::class, [
                'class' => InvoiceAccountingType::class,
                'choice_value' => 'id',
                'choice_label' => 'type_name',
                'label_attr' => [
                    'class' => 'btn btn-outline-primary radio-margin-negate'
                ],
                'choice_attr' => function($choice, $key, $value) {
                    // adds a class like attending_yes, attending_no, etc
                    return ['class' => 'btn-check'];
                },
                'expanded' => true,
                'multiple' => false,
            ])
            ->add('contact', ChoiceType::class, [
                'choices' => $this->contacts,
                'choice_value' => 'id',
                'choice_label' => 'title',
            ])
            ->add('payment_method', EntityType::class, [
                'class' => Payment::class,
                'choice_value' => 'id',
                'choice_label' => 'type',
                'label_attr' => [
                    'class' => 'btn btn-outline-primary radio-margin-negate'
                ],
                'choice_attr' => function($choice, $key, $value) {
                    // adds a class like attending_yes, attending_no, etc
                    return ['class' => 'btn-check'];
                },
                'expanded' => true,
                'multiple' => false,
            ])
            ->add('invoice_number')
            ->add('currency', EntityType::class, [
                'class' => Currency::class,
                'choice_value' => 'id',
                'choice_label' => 'name',
                'expanded' => false,
                'multiple' => false,
            ])
            ->add('variable_symbol')
            ->add('text_before', TextareaType::class, [
                'required' => false,
            ])
            ->add('invoiceItems', CollectionType::class, [
                'entry_type' => InvoiceItemFormType::class,
                'allow_add' => true,
                'allow_delete' => true,
            ])
//            ->add('newItem', ButtonType::class, [
//                'attr' => [
//                    'class' => "btn btn-primary btn-lg btn-block",
//                    'style' => "background-color:#04AA6D;border:1px solid #04AA6D",
//                ]
//            ])
            ->add('price', IntegerType::class, [
                'attr'=> [
                    'class' => 'form-control-plaintext',
                    'style' => 'background-color: transparent; text-align: right;',
                    'readonly' => '',
                ]
            ])
            ->add('text_after', TextareaType::class, [
                'required' => false,
            ])
            ->add('note', TextareaType::class, [
                'required' => false,
            ])
            ->add('footer_text', TextareaType::class, [
                'required' => false
            ])
            ->add('create_date', DateType::class, [
                'widget' => 'single_text',
//                'input'  => 'datetime_immutable',
                'format' => 'dd-MM-yyyy',
                'attr' => [
                    'class' => 'js-datepicker'
                ],
                 'html5' => false,
            ])
            ->add('due_date', DateType::class, [
                'widget' => 'single_text',
//                'input'  => 'datetime_immutable',
                'format' => 'dd-MM-yyyy',
                'attr' => [
                    'class' => 'js-datepicker'
                ],
                'html5' => false,
            ])
            ->add('tax_application_date', DateType::class, [
                'widget' => 'single_text',
//                'input'  => 'datetime_immutable',
                'format' => 'dd-MM-yyyy',
                'attr' => [
                    'class' => 'js-datepicker'
                ],
                'html5' => false,
            ])
            ->add('submit', SubmitType::class, [
                'attr' => [
                    'class' => "btn btn-primary btn-lg btn-block",
                ]
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Invoice::class,
            'contacts' => null,
        ]);
    }
}
