<?php

namespace App\Form;

use App\Entity\InvoiceItem;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class InvoiceItemFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('name', null, [
                'attr' => [
                    'placeholder' => 'Zadejte prosím název položky faktury'
                ]
            ])
            ->add('price', IntegerType::class, [
                'attr'=> [
                    'class' => 'form-control-plaintext single-item-price',
                    'style' => 'background-color: transparent; text-align: right;',
                    'readonly' => '',
                ]
            ])
            ->add('unit', TextType::class)
            ->add('amount', IntegerType::class)
            ->add('pricePerUnit', IntegerType::class)
//            ->add('invoice')
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => InvoiceItem::class,
        ]);
    }
}
