<?php

namespace App\Entity;

use App\Repository\InvoiceRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=InvoiceRepository::class)
 */
class Invoice
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $invoice_number;


    /**
     * @ORM\ManyToOne(targetEntity=Contact::class, inversedBy="invoices",cascade={"persist"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $contact;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $variable_symbol;

    /**
     * @ORM\ManyToOne(targetEntity=Payment::class, inversedBy="invoices")
     * @ORM\JoinColumn(nullable=false)
     */
    private $payment_method;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $text_before;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $text_after;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $note;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $footer_text;

    /**
     * @ORM\ManyToOne(targetEntity=InvoiceType::class)
     * @ORM\JoinColumn(nullable=false)
     */
    private $invoice_type;

    /**
     * @ORM\ManyToOne(targetEntity=InvoiceAccountingType::class)
     * @ORM\JoinColumn(nullable=false)
     */
    private $InvoiceAccountingType;

    /**
     * @ORM\Column(type="date")
     */
    private $create_date;

    /**
     * @ORM\Column(type="date")
     */
    private $due_date;

    /**
     * @ORM\Column(type="date")
     */
    private $tax_application_date;

    /**
     * @ORM\ManyToOne(targetEntity=InvoiceStatus::class, inversedBy="invoices")
     * @ORM\JoinColumn(nullable=false)
     */
    private $status;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="invoices")
     * @ORM\JoinColumn(nullable=false)
     */
    private $user;

    /**
     * @ORM\ManyToOne(targetEntity=Currency::class, inversedBy="invoices")
     * @ORM\JoinColumn(nullable=false)
     */
    private $currency;

    /**
     * @ORM\OneToMany(targetEntity=InvoiceItem::class, mappedBy="invoice", orphanRemoval=true, cascade={"persist"})
     */
    private $invoiceItems;

    /**
     * @ORM\Column(type="float")
     */
    private $price;


    public function __construct()
    {
        $this->invoice_item_id = new ArrayCollection();
        $this->invoiceItems = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getInvoiceNumber(): ?string
    {
        return $this->invoice_number;
    }

    public function setInvoiceNumber(string $invoice_number): self
    {
        $this->invoice_number = $invoice_number;

        return $this;
    }


    public function getContact(): ?Contact
    {
        return $this->contact;
    }

    public function setContact(?Contact $contact): self
    {
        $this->contact = $contact;

        return $this;
    }

    public function getVariableSymbol(): ?string
    {
        return $this->variable_symbol;
    }

    public function setVariableSymbol(string $variable_symbol): self
    {
        $this->variable_symbol = $variable_symbol;

        return $this;
    }

    public function getPaymentMethod(): ?Payment
    {
        return $this->payment_method;
    }

    public function setPaymentMethod(?Payment $payment_method): self
    {
        $this->payment_method = $payment_method;

        return $this;
    }

    public function getTextBefore(): ?string
    {
        return $this->text_before;
    }

    public function setTextBefore(?string $text_before): self
    {
        $this->text_before = $text_before;

        return $this;
    }

    public function getTextAfter(): ?string
    {
        return $this->text_after;
    }

    public function setTextAfter(?string $text_after): self
    {
        $this->text_after = $text_after;

        return $this;
    }


    public function getNote(): ?string
    {
        return $this->note;
    }

    public function setNote(?string $note): self
    {
        $this->note = $note;

        return $this;
    }

    public function getFooterText(): ?string
    {
        return $this->footer_text;
    }

    public function setFooterText(?string $footer_text): self
    {
        $this->footer_text = $footer_text;

        return $this;
    }

    public function getInvoiceType(): ?InvoiceType
    {
        return $this->invoice_type;
    }

    public function setInvoiceType(?InvoiceType $invoice_type): self
    {
        $this->invoice_type = $invoice_type;

        return $this;
    }

    public function getInvoiceAccountingType(): ?InvoiceAccountingType
    {
        return $this->InvoiceAccountingType;
    }

    public function setInvoiceAccountingType(?InvoiceAccountingType $InvoiceAccountingType): self
    {
        $this->InvoiceAccountingType = $InvoiceAccountingType;

        return $this;
    }

    public function getCreateDate(): ?\DateTimeInterface
    {
        return $this->create_date;
    }

    public function setCreateDate(\DateTimeInterface $create_date): self
    {
        $this->create_date = $create_date;

        return $this;
    }

    public function getDueDate(): ?\DateTimeInterface
    {
        return $this->due_date;
    }

    public function setDueDate(\DateTimeInterface $due_date): self
    {
        $this->due_date = $due_date;

        return $this;
    }

    public function getTaxApplicationDate(): ?\DateTimeInterface
    {
        return $this->tax_application_date;
    }

    public function setTaxApplicationDate(\DateTimeInterface $tax_application_date): self
    {
        $this->tax_application_date = $tax_application_date;

        return $this;
    }

    public function getStatus(): ?InvoiceStatus
    {
        return $this->status;
    }

    public function setStatus(?InvoiceStatus $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getCurrency(): ?Currency
    {
        return $this->currency;
    }

    public function setCurrency(?Currency $currency): self
    {
        $this->currency = $currency;

        return $this;
    }

    /**
     * @return Collection|InvoiceItem[]
     */
    public function getInvoiceItems(): Collection
    {
        return $this->invoiceItems;
    }

    public function addInvoiceItem(InvoiceItem $invoiceItem): self
    {
        if (!$this->invoiceItems->contains($invoiceItem)) {
            $this->invoiceItems[] = $invoiceItem;
            $invoiceItem->setInvoice($this);
        }

        return $this;
    }

    public function removeInvoiceItem(InvoiceItem $invoiceItem): self
    {
        if ($this->invoiceItems->removeElement($invoiceItem)) {
            // set the owning side to null (unless already changed)
            if ($invoiceItem->getInvoice() === $this) {
                $invoiceItem->setInvoice(null);
            }
        }

        return $this;
    }

    public function getPrice(): ?float
    {
        return $this->price;
    }

    public function setPrice(float $price): self
    {
        $this->price = $price;

        return $this;
    }

}
