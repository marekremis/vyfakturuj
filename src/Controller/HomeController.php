<?php

namespace App\Controller;

use App\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class HomeController extends AbstractController
{
    /**
     * @Route("/", name="home")
     */
    public function index(Request $request): Response
    {
        if($this->isGranted('ROLE_USER')) {
            return $this->redirect($this->generateUrl(route: 'app.home'));
        }

            return $this->render('home/index.html.twig', [
                'controller_name' => 'HomeController',
            ]);
    }
}
