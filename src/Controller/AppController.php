<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/app', name: 'app.')]
class AppController extends AbstractController
{
    private $requestStack;

    public function __construct(RequestStack $requestStack)
    {
        $this->requestStack = $requestStack;
    }

    #[Route('/', name: 'home')]
    public function index(): Response
    {
        $session = $this->requestStack->getSession();

        // stores an attribute in the session for later reuse
        $session->set('foo', 'attribute-value');

        // gets an attribute by name
        $foo = $session->get('foo');


        return $this->render('app/index.html.twig', [
            'controller_name' => 'AppController',
        ]);
    }
}
