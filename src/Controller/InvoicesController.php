<?php

namespace App\Controller;

use App\Entity\Contact;
use App\Entity\Currency;
use App\Entity\Invoice;
use App\Entity\InvoiceAccountingType;
use App\Entity\InvoiceItem;
use App\Entity\InvoiceStatus;
use App\Entity\InvoiceType;
use App\Entity\Payment;
use App\Form\InvoiceFormType;
use App\Repository\InvoiceRepository;
use Dompdf\Css\Style;
use Dompdf\Css\Stylesheet;
use Dompdf\Dompdf;
use Dompdf\Options;
use mysql_xdevapi\Exception;
use phpDocumentor\Reflection\Types\Integer;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\User\UserInterface;

#[Route('/invoices', name: 'invoices.')]
class InvoicesController extends AbstractController
{
    private Invoice $invoice;
    private string $projectDir;

    public function __construct(string $projectDir)
    {
        $this->projectDir = $projectDir;
    }

    #[Route('/', name: 'home')]
    public function index(InvoiceRepository $invoiceRepository, UserInterface $user): Response
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');

        $invoices = $invoiceRepository->findBy(
            ['user' => $user->getId()]
        );

        return $this->render('invoices/index.html.twig', [
            'invoices' => $invoices,
        ]);
    }

    #[Route('/create', name: 'create')]
    public function create(UserInterface $user, Request $request): Response
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');

        // Create instance and fill with default values so they are prefilled in form
        $newInvoice = $this->generateNewInvoiceWithDefaultValues();


        // Contacts related to current User
        $contacts = $this->getDoctrine()
            ->getRepository(Contact::class)
            ->findBy(
                ['user' => $user->getId()]
            )
        ;

        $form = $this->createForm(InvoiceFormType::class, $newInvoice, [
            'action' => $this->generateUrl('invoices.create'),
            'contacts' => $contacts,
        ]);

        $form->handleRequest($request);

        // if Submited and valid form then
        if($form->isSubmitted() && $form->isValid()) {
            //Fill Contact with rest of data from form
            $this->invoice = $form->getData();

            //Set relation to this invoice for each invoiceItem
            foreach ($this->invoice->getInvoiceItems() as $invoiceItem) {
                $invoiceItem->setInvoice($this->invoice);
            }

            //Assign relation with current user
            $this->invoice->setUser($user);


            //Entity Manager
            $em = $this->getDoctrine()->getManager();

            $em->persist($this->invoice);
            $em->flush();

            $this->addFlash('success', 'Nová faktura vytvořena');

            return $this->redirect($this->generateUrl("invoices.home"));
        }


        return $this->render('invoices/create.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    #[Route('/edit/{id}', name: 'edit')]
    public function edit($id): Response
    {
        return $this->render('invoices/index.html.twig', [
            'controller_name' => 'InvoicesController',
        ]);
    }

    #[Route('/remove/{id}', name: 'remove')]
    public function remove($id, InvoiceRepository $invoiceRepository): Response
    {
        try {
            $invoice = $invoiceRepository->find($id);
        }
        catch (Exception $exception) {
            dump($exception . " Invoice with id:" . $id . " does not exist");
        }

        $em = $this->getDoctrine()->getManager();

        $em->remove($invoice);
        $em->flush();

        return $this->redirect($this->generateUrl("invoices.home"));
    }

    #[Route('/generate/{id}', name: 'generate')]
    public function generateHtmlInvoice($id, InvoiceRepository $invoiceRepository, UserInterface $user): Response
    {
        try {
            $invoice = $invoiceRepository->find($id);
        }
        catch (Exception $exception) {
            dump($exception . " Invoice with id:" . $id . " does not exist");
        }


        $pdfOptions = new Options();
        $pdfOptions->set('defaultFont', 'Arial');
        $pdfOptions->set('isRemoteEnabled', true);

        $dompdf = new Dompdf($pdfOptions);


        $html = $this->renderView('invoices/invoice.html.twig', [
            'invoice' => $invoice,
            'user' => $user,
        ]);

        $dompdf->loadHtml($html);

        $dompdf->setPaper('A4');

        $dompdf->render();

        // Parameters
        $x          = 275;
        $y          = 820;
        $text       = "Strana {PAGE_NUM} / {PAGE_COUNT}";
        $font       = $dompdf->getFontMetrics()->get_font('Helvetica', 'normal');
        $size       = 10;
        $color      = array(0,0,0);
        $word_space = 0.0;
        $char_space = 0.0;
        $angle      = 0.0;

        $dompdf->getCanvas()->page_text(
            $x, $y, $text, $font, $size, $color, $word_space, $char_space, $angle
        );

        $dompdf->stream("mypdf.pdf", [
            "Attachment" => false,
        ]);

        return $this->render('invoices/invoice.html.twig', [
            'invoice' => $invoice,
            'user' => $user,
        ]);
    }

    private function generateNewInvoiceWithDefaultValues(): Invoice
    {
        $invoice = new Invoice();
        $invoiceItem = new InvoiceItem();
        $invoiceItem->setName("");
        $invoiceItem->setPrice(0);
        $invoiceItem->setPricePerUnit(0);
        $invoiceItem->setUnit('kg');
        $invoiceItem->setAmount(1);



        $invoiceAccountingType = $this->getDoctrine()
            ->getRepository(InvoiceAccountingType::class)
            ->findOneBy(
                ['isDefault' => true]
            )
        ;
        $invoiceType = $this->getDoctrine()
            ->getRepository(InvoiceType::class)
            ->findOneBy(
                ['isDefault' => true]
            )
        ;
        $invoiceStatus = $this->getDoctrine()
            ->getRepository(InvoiceStatus::class)
            ->findOneBy(
                ['isDefault' => true]
            )
        ;
        $invoicePayment = $this->getDoctrine()
            ->getRepository(Payment::class)
            ->findOneBy(
                ['isDefault' => true]
            )
        ;
        $invoiceCurrency = $this->getDoctrine()
            ->getRepository(Currency::class)
            ->findOneBy(
                ['isDefault' => true]
            )
        ;
        $dateTimeNow = new \DateTime('now');
        $dateTimeTwoWeeksFromNow = (new \DateTime('now'))->add(new \DateInterval('P30D'));
        $lastInvoiceNumber = $this->getLastInvoiceNumberPlusOne();

        $invoice->setCurrency($invoiceCurrency);
        $invoice->setInvoiceAccountingType($invoiceAccountingType);
        $invoice->setInvoiceType($invoiceType);
        $invoice->setStatus($invoiceStatus);
        $invoice->setPaymentMethod($invoicePayment);
        $invoice->addInvoiceItem($invoiceItem);
        $invoice->setCreateDate($dateTimeNow);
        $invoice->setDueDate($dateTimeTwoWeeksFromNow);
        $invoice->setTaxApplicationDate($dateTimeNow);
        $invoice->setPrice(0);
        $invoice->setInvoiceNumber($lastInvoiceNumber);
        $invoice->setVariableSymbol($lastInvoiceNumber);



        return $invoice;
    }

    public function getLastInvoiceNumberPlusOne() :string {
        $em = $this->getDoctrine()->getManager();

        $QUERY = 'SELECT invoice_number FROM invoice WHERE user_id= :id ORDER BY id DESC LIMIT 1';
        $statement = $em->getConnection()->prepare($QUERY);
        $statement->bindValue('id', $this->getUser()->getId());
        $statement->execute();

        $result = $statement->fetchAll();

        // String value
        $stringValueOfInvoiceNumber = $result[0]['invoice_number'];

        $num = intval($stringValueOfInvoiceNumber);
        $num++;

       return strVal($num);
    }
}
