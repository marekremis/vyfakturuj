<?php

namespace App\Controller;

use App\Entity\Contact;
use App\Form\ContactFormType;
use App\Repository\ContactRepository;
use mysql_xdevapi\Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\FormBuilder;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\User\UserInterface;

#[Route('/contacts', name: 'contacts.')]
class ContactsController extends AbstractController
{
    #[Route('/', name: 'home')]
    public function index(ContactRepository $contactRepository, UserInterface $user): Response
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');

        $contacts = $contactRepository->findBy(
            ['user' => $user->getId()]
        );

        return $this->render('contacts/index.html.twig', [
            'contacts' => $contacts,
        ]);
    }

    #[Route('/create', name: 'create')]
    public function create(Request $request): Response
    {

        $contact = new Contact();

        $form = $this->createForm(ContactFormType::class, $contact, [
            'action' => $this->generateUrl('contacts.create')
        ]);

        $form->handleRequest($request);

        // if Submited and valid form then
        if($form->isSubmitted() && $form->isValid()) {
            //Assign relation with current usser
            $contact->setUser($this->getUser());

            //Fill Contact with rest of data from form
            $contact = $form->getData();

            //Entity Manager
            $em = $this->getDoctrine()->getManager();

            $em->persist($contact);
            $em->flush();

            $this->addFlash('success', 'Nový kontakt úspěšně uložen');

            return $this->redirect($this->generateUrl("contacts.home"));
        }

        return $this->render('contacts/create.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    #[Route('/edit/{id}', name: 'edit')]
    public function edit(Request $request, $id, ContactRepository $contactRepository): Response
    {
        $contact = $contactRepository->find($id);


        $form = $this->createForm(ContactFormType::class, $contact, [
            'action' => $this->generateUrl('contacts.edit', [
                'id' => $id
            ])
        ]);

        $form->handleRequest($request);

        // if Submited and valid form then
        if($form->isSubmitted() && $form->isValid()) {

            //Fill Contact with rest of data from form
            $contact = $form->getData();

            //Entity Manager
            $em = $this->getDoctrine()->getManager();

            $em->persist($contact);
            $em->flush();

            $this->addFlash('success', 'Kontakt úspěšně upraven');

            return $this->redirect($this->generateUrl("contacts.home"));
        }

        return $this->render('contacts/edit.html.twig', [
            'form' => $form->createView(),
        ]);
    }


    #[Route('/remove/{id}', name: 'remove')]
    public function remove($id, ContactRepository $contactRepository, UserInterface $user): Response
    {


        try {
            $contact = $contactRepository->find($id);
        }
        catch (Exception $exception) {
            dump($exception . " Contact with id:" . $id . " does not exist");
        }

        //Entity Manager
        $em = $this->getDoctrine()->getManager();

        $em->remove($contact);
        $em->flush();

        return $this->redirect($this->generateUrl("contacts.home"));
    }



}
