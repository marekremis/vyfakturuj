$(document).ready(function () {

    // Add new InvoiceItem
    var itemRowNumber = $('#invoice_items').children().last().data('rownum');
    $('#invoice_form_newItem').click(function () {
        itemRowNumber++;
        $('#invoice_items').append('<div class="row" id="item' + itemRowNumber +'" data-rownum="' + itemRowNumber +'" style="padding-top: 3rem">\n' +
            '                            <div class="col col-md-6">\n' +
            '                                <label for="invoice_form_invoiceItems_' + itemRowNumber +'_name" class="form-label required">Název</label>\n' +
            '                                <input type="text" id="invoice_form_invoiceItems_' + itemRowNumber +'_name" name="invoice_form[invoiceItems][' + itemRowNumber +'][name]" required="required" maxlength="255" placeholder="Zadejte prosím název položky faktury" class="form-control">\n' +
            '                            </div>\n' +
            '                            <div class="col col-md-1">\n' +
            '                                <label for="invoice_form_invoiceItems_' + itemRowNumber +'_amount" class="form-label required">Množství</label>\n' +
            '                                <input type="number" id="invoice_form_invoiceItems_' + itemRowNumber +'_amount" name="invoice_form[invoiceItems][' + itemRowNumber +'][amount]" required="required" class="form-control" value="1">\n' +
            '                            </div>\n' +
            '                            <div class="col col-md-1">\n' +
            '                                <label for="invoice_form_invoiceItems_' + itemRowNumber +'_unit" class="form-label required">Jednotka</label>\n' +
            '                                <input type="text" id="invoice_form_invoiceItems_' + itemRowNumber +'_unit" name="invoice_form[invoiceItems][' + itemRowNumber +'][unit]" required="required" class="form-control" value="kg">\n' +
            '                            </div>\n' +
            '                            <div class="col col-md-1 form-label-a-right">\n' +
            '                                <label for="invoice_form_invoiceItems_' + itemRowNumber +'_pricePerUnit" class="form-label required" >Cena</label>\n' +
            '                                <input type="number" id="invoice_form_invoiceItems_' + itemRowNumber +'_pricePerUnit" name="invoice_form[invoiceItems][' + itemRowNumber +'][pricePerUnit]" required="required" class="form-control"  placeholder="CZK">\n' +
            '                            </div>\n' +
            '                            <div class="col col-md-2">\n' +
            '                                <div class="form-label form-label-a-right">Celkem</div>\n' +
            '                                <span> <input type="number" id="invoice_form_invoiceItems_' + itemRowNumber +'_price" name="invoice_form[invoiceItems][' + itemRowNumber +'][price]" required="required" class="form-control-plaintext single-item-price form-control" style="background-color: transparent; text-align: right;" readonly="" value="0"></span>\n' +
            '                            </div>\n' +
            '                            <div class="col col-md-1">\n' +
            '                                <label class="form-label"></label>\n' +
            '                                <i id="removeItem" class="fas fa-minus-circle fa-2x" style="display: block; color: #d11a2a; padding-top: 10px" data-toggle="tooltip" data-placement="top" title="Odstranit položku"></i>\n' +
            '                            </div>\n' +
            '                        </div>')
    });

    // Remove item row
    $('#invoice_items').on('click', '#removeItem', function () {
        $(this).closest('.row').remove();
    });

    $('#invoice_items').on('change', 'input', function () {
        var currentRow = $(this).closest('.row');
        var rowNum = currentRow.attr('data-rownum');

        updateInvoiceItemPrice(rowNum, currentRow);
    })

})

function updateInvoiceItemPrice (rowNum, currentRow) {
    var amountInputNumber = currentRow.find('#invoice_form_invoiceItems_' + rowNum + '_amount').val();
    var pricePerUnitInputNumber = currentRow.find('#invoice_form_invoiceItems_' + rowNum + '_pricePerUnit').val();
    var priceInputObject = currentRow.find('#invoice_form_invoiceItems_' + rowNum + '_price');

    priceInputObject.val(amountInputNumber * pricePerUnitInputNumber);
    updateInvoicePrice();
}

function updateInvoicePrice () {
    var price = parseInt(0);

    $('.single-item-price').each(function() {
        price = price + parseInt($(this).val());
    });


    $('#invoice_form_price').val(price);
}