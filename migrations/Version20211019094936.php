<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20211019094936 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE invoice_item DROP FOREIGN KEY FK_1DDE477BF476E05C');
        $this->addSql('DROP TABLE unit');
        $this->addSql('DROP INDEX IDX_1DDE477BF476E05C ON invoice_item');
        $this->addSql('ALTER TABLE invoice_item ADD unit VARCHAR(255) DEFAULT NULL, DROP unit_id_id');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE unit (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`, unit_name VARCHAR(255) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('ALTER TABLE invoice_item ADD unit_id_id INT NOT NULL, DROP unit');
        $this->addSql('ALTER TABLE invoice_item ADD CONSTRAINT FK_1DDE477BF476E05C FOREIGN KEY (unit_id_id) REFERENCES unit (id)');
        $this->addSql('CREATE INDEX IDX_1DDE477BF476E05C ON invoice_item (unit_id_id)');
    }
}
